
let mark = 78;

if(mark >= 90){
    console.log("The grade is A+")
}
else if(mark >= 80){
    console.log("The grade is A")
}
else if(mark >= 70){
    console.log("The grade is B+")
}
else if(mark >= 60){
    console.log("The grade is B")
}
else if(mark >= 50){
    console.log("The grade is C+")
}
else{
    console.log("Failed")
}
